console.log("background script loaded");

chrome.runtime.onInstalled.addListener(function() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {
                    hostContains: 'artofproblemsolving.com',
                    pathContains: 'texer'
                }
            })
        ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }])
    })
});