class DelayedAction {
    constructor(window, interval, callback) {
        this.window = window;
        this.interval = interval;
        this.timer = null;
        this.callback = callback;
    }

    clear() {
        this.window.clearTimeout(this.timer);
    }

    activate() {
        this.clear();
        this.timer = this.window.setTimeout(this.callback, this.interval);
    }
}

const saveButton = document.getElementById("save");
const renderButton = document.getElementById("render-bbcode");

const typingAction = new DelayedAction(window, 5000, function () {
    console.log("Auto saving");
    saveButton.click();
});

const renderAction = new DelayedAction(window, 2000, function () {
    console.log("Auto rendering");
    renderButton.click();
})

window.addEventListener("keyup", function (event) {
    typingAction.activate();
    renderAction.activate();
});

window.addEventListener("keydown", function (event) {
    typingAction.clear();
    renderAction.clear();
});

// aops flyout and modal wrapper logic

function getElementFromNode(node) {
    if (!node.hasChildNodes()) {
        node.appendChild(document.createElement(null));
    }
    return node.firstChild.parentElement;
}

const observer = new MutationObserver(function (mutations) {
    mutations.forEach((mutation) => {
        for (let node of mutation.addedNodes) {
            let element = getElementFromNode(node);
            console.log(element);

            if (element.className == 'aops-modal-mask' || element.id == 'flyout') {
                node.parentNode.removeChild(node);
            } else if (element.className == 'aops-modal-wrapper') {
                let contents = document.getElementsByClassName('aops-modal-body')[0];
                let text = contents.innerText;
                node.parentNode.removeChild(node);
                console.log(text);
            }
        }
    })
});

observer.observe(document.body, { childList: true });